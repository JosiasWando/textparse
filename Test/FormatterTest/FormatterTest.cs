using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Src.Util;
using Xunit;

namespace Test.FormatterTest
{
    public class FormatterTest
    {
        [Fact]
        public void CanUpperString()
        {
            Formatter f = new();
            FormatterUpper up = new(f);

            string src = "loren ipson et donet";

            var upperSrc = up.Formatar(src);

            Assert.Equal("LOREN IPSON ET DONET", upperSrc);
        }

        [Fact]
        public void CanTrimString()
        {
            Formatter f = new();
            FormatterTrim t = new(f);

            string src = "loren ipson et donet";

            var trimSrc = t.Formatar(src);

            Assert.Equal("lorenipsonetdonet", trimSrc);
        }

        [Fact]
        public void CanTrimAndUpperString()
        {
            Formatter f = new();
            FormatterTrim t = new(f);
            FormatterUpper up = new(t);

            string src = "loren ipson et donet";

            var trimSrc = up.Formatar(src);

            Assert.Equal("LORENIPSONETDONET", trimSrc);
        }

        [Fact]
        public void CanTrimAndUpperAndRemoveSpecialString()
        {
            Formatter f = new();
            FormatterTrim t = new(f);
            FormatterUpper up = new(t);
            FormatterSpecial sp = new (up);
            string src = "loren ipsôn et donet aça";

            var trimSrc = sp.Formatar(src);

            Assert.Equal("LORENIPSONETDONETACA", trimSrc);
        }
    }
}
