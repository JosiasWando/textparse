using System;
using Src.Interface;

namespace Src.Util
{
    public class FormatterUpper : IFormatter
    {
        readonly IFormatter Formatter;
        public FormatterUpper(IFormatter f)
        {
            Formatter = f;
        }
        public string Formatar(string txt)
        {
            return Formatter.Formatar(txt).ToUpper();
        }
    }
}