using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Src.Interface;

namespace Src.Util
{
    public class FormatterSpecial : IFormatter
    {
        readonly IFormatter Formatter;

        public FormatterSpecial(IFormatter formatter)
        {
            Formatter = formatter;
        }

        public string Formatar(string txt)
        {
            var src = Formatter.Formatar(txt);
            return RemoveDiacritics(src);
        }

        private static string RemoveDiacritics(string text)
        {
            var normalizedString = text.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}