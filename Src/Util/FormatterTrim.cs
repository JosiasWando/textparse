using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Src.Interface;

namespace Src.Util
{
    public class FormatterTrim: IFormatter
    {
        readonly IFormatter Formatter;
        public FormatterTrim(IFormatter f)
        {
            Formatter = f;
        }

        public string Formatar(string txt)
        {
            return Formatter.Formatar(txt).Trim().Replace(" ", "");
        }
    }
}